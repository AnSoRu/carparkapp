package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class PlainMysqlDatabaseDriver {

	private static final String HOST_NAME = "sql7.freesqldatabase.com";
	private static final String SERVER_PORT = "3306";
	private static final String DATABASE = "sql7141746";
	private static final String USERNAME = "sql7141746";
	private static final String PASSWORD = "KQpJEAb3JC";

	public Connection getConnection() throws SQLException, ClassNotFoundException {

		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", USERNAME);
		connectionProps.put("password", PASSWORD);

		conn = (Connection) DriverManager
				.getConnection("jdbc:mysql://" + HOST_NAME + ":" + SERVER_PORT + "/" + DATABASE, connectionProps);

		System.out.println("Connected to database");
		return conn;
	}
}
