package ws;

import javax.ws.rs.Path;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import database.PlainMysqlDatabaseDriver;
import email.SendEmail;

@Path("/account")
public class AccountService {

	static final Logger asLogger = LogManager.getLogger(AccountService.class.getName());
	protected Connection dbConn;
	

	@GET
	@Produces("application/json")

	public String register(
			@QueryParam("firstName") String firstName, // 0
			@QueryParam("lastName") String lastName, // 1
			@QueryParam("personalIdType") String personalIdType, // 2
			@QueryParam("personalId") String personalId, // 3
			@QueryParam("profession") String profession, // 4
			@QueryParam("dob") String dob, // 5
			@QueryParam("email") String email, // 6
			@QueryParam("phone") String phone, // 7
			@QueryParam("nationality") String nationality, // 8
			@QueryParam("password") String password // 9
	) {

		if (firstName == null || lastName == null || personalIdType == null || personalId == null || profession == null
				|| dob == null || email == null || phone == null || nationality == null || password == null) {

			asLogger.error("Missing required attributes");
			return "Missing required attributes";
		}

		try {
			dbConn = (new PlainMysqlDatabaseDriver()).getConnection();

			AuthUtil authUtil = new AuthUtil();
			String passwordHash = authUtil.hash(password.toCharArray());

			String[] accountProperties = { firstName, lastName, personalIdType, personalId, profession, dob, email,
					phone, nationality, passwordHash };

			if (saveUser(accountProperties)) {
				SendEmail sm = new SendEmail();
				sm.SendMail(email, "CarPark user registration confirmation", "Hi " + firstName + ",\nThanks for register CarPark.\nYour username is " + email +"\nPlease go to the login form on your mobile application and enter your credentials.\nHope you enjoy this app.\n\nYour friends,\nCarPark Developper Team.");
				return "OK";
			} else {
				return "Not valid credentials";
			}

		} catch (ParseException pe) {
			asLogger.error("Failed to parse supplied date", pe);
			return "Failed to parse supplied date";
		} catch (Exception e) {
			asLogger.error("Failed to register new user", e);
			return "Failed to register new user";
		}
	}
	

	protected boolean saveUser(String[] accountProperties) throws SQLException, ParseException, ClassNotFoundException {

		/*
		 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 * Date parsedDate = dateFormat.parse(accountProperties[5]); Timestamp
		 * timestamp = new Timestamp(parsedDate.getTime());
		 */

		String insertTableSQL = "INSERT INTO User " + "(`first_name`, `last_name`, `personal_id_type`, `personal_id`, "
				+ "`profession`, `dob`, `email`, `phone`, `nationality`, `password`)  "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = dbConn.prepareStatement(insertTableSQL);
		preparedStatement.setString(1, accountProperties[0]);
		preparedStatement.setString(2, accountProperties[1]);
		preparedStatement.setString(3, accountProperties[2]);
		preparedStatement.setString(4, accountProperties[3]);
		preparedStatement.setString(5, accountProperties[4]);
		preparedStatement.setString(6, accountProperties[5]);
		preparedStatement.setString(7, accountProperties[6]);
		preparedStatement.setString(8, accountProperties[7]);
		preparedStatement.setString(9, accountProperties[8]);
		preparedStatement.setString(10, accountProperties[9]);

		// execute insert SQL stetement
		int rowCount = preparedStatement.executeUpdate();

		return rowCount == 1;
	}
	
	/*protected ResultSet getUser(String [] accountProperties) throws SQLException{
		//int rowCount = 0;
		String getTableSQL;
		PreparedStatement preparedStatement = null;
		if(accountProperties.length > 0){
			String email = accountProperties[0];
			String pass = accountProperties[1];
			
			getTableSQL = "SELECT `email` ,`password` FROM User WHERE email = ? AND password = ?";
				
			    preparedStatement = dbConn.prepareStatement(getTableSQL);
				preparedStatement.setString(1,email);
				preparedStatement.setString(2,pass);
			
		}
		return preparedStatement.executeQuery();

	}*/
	
	private ResultSet getUser(String username) throws SQLException {
		String query = "SELECT `id`, `password` " + "FROM User WHERE email = ? LIMIT 1";
		System.out.println(query);

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setString(1, username);

		return preparedStatement.executeQuery();
	}
	
	@Path("/login")
	@GET
	public String accountLogin(
			@QueryParam("email") String email,
			@QueryParam("password") String pass){
		
		if(email == null || pass == null ){
			return "Missing required attributes";
		}
		try {
			dbConn = (new PlainMysqlDatabaseDriver()).getConnection();

			ResultSet userResult = getUser(email);
			String passwordHash = "";
			
			int userId = -1;
			if (userResult.first()) {
				passwordHash = userResult.getString("password");
				userId = userResult.getInt("id");
			}

			AuthUtil authUtil = new AuthUtil();
			if (!authUtil.authenticate(pass.toCharArray(), passwordHash)) {
				return "Not valid credentials";
			}
			else{
				return "OK";
			}
			

		} catch (Exception e) {
			asLogger.error("Failed to login", e);
			return "Failed to login";
		}
	}

	
	
	

}
