package ws;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import database.PlainMysqlDatabaseDriver;
import flexjson.JSONSerializer;

@Path("/carpark")
public class CarParkService {

	static final Logger cpsLogger = LogManager.getLogger(CarParkService.class.getName());
	protected Connection dbConn;

	@GET
	@Produces("application/json")
	public String list(@QueryParam("lat") Double latitude, @QueryParam("long") Double longitude,
			@QueryParam("radius") Integer radius, @QueryParam("max_price") Double maxPrice,
			@QueryParam("arrival_datetime") String arrivalDateTime) {

		if (latitude == null || longitude == null) {

			cpsLogger.error("Missing required attributes");
			return "Missing required attributes";
		}

		try {
			dbConn = (new PlainMysqlDatabaseDriver()).getConnection();

			ResultSet results = buildSearchQuery(latitude, longitude, radius, maxPrice, arrivalDateTime).executeQuery();
			List<CarParkWrapper> carParks = new ArrayList<CarParkWrapper>();

			while (results.next()) {
				CarParkWrapper cp = new CarParkWrapper();
				cp.id = results.getInt("id");
				cp.latitude = results.getDouble("lat");
				cp.longitude = results.getDouble("long");
				cp.capacity = results.getInt("capacity");
				cp.price_per_minute = results.getDouble("price_per_minute");
				cp.address = results.getString("address");
				cp.distance_to_destination = results.getDouble("distance_to_destination");
				cp.available = results.getInt("available");

				carParks.add(cp);
			}

			return new JSONSerializer().serialize(carParks);
			} catch (Exception e) {
			cpsLogger.error("Failed to list car parks", e);
			return "Failed to list car parks";
		}
	}

	protected PreparedStatement buildSearchQuery(Double latitude, Double longitude, Integer radius, Double maxPrice,
			String arrivalDateTime) throws ClassNotFoundException, SQLException {

		String query = "SELECT `id`, `lat`, `long`, `capacity`, `price_per_minute`, `address`, "
				+ "        ROUND(GEO_DISTANCE(Point(`lat`, `long`), Point(?, ?))*1000) AS `distance_to_destination`, "
				+ "        (`capacity` - IFNULL( bb.`spaces_taken` , 0 )) AS `available` " + "FROM CarPark cp ";

		// If no arrivalDateTime supplied, should check availability at the
		// given moment in time
		if (arrivalDateTime == null || arrivalDateTime.isEmpty()) {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");// dd/MM/yyyy
			Date now = new Date();
			arrivalDateTime = sdfDate.format(now);
		}

		String join = "LEFT JOIN (SELECT COUNT(b.`id`) as `spaces_taken`, b.`CarPark_id` " + "FROM Booking b "
				+ "WHERE (b.`booked_from` <= STR_TO_DATE(?,'%Y-%m-%dT%H:%i') "
				+ "AND b.`booked_to` > STR_TO_DATE(?,'%Y-%m-%dT%H:%i')) " + "GROUP BY b.`CarPark_id`) bb "
				+ "ON bb.`CarPark_id` = cp.`id`";

		String where = "";
		String having = "";
		String limit = "";
		String order = "";

		if (radius != null) {
			having = " HAVING `distance_to_destination` <= ? ";
		} else {
			limit = " LIMIT 5 ";
		}

		if (maxPrice != null) {
			where = " WHERE `price_per_minute` <= ? ";
		}

		order = " ORDER BY `distance_to_destination` ASC ";

		query = query + join + where + having + order + limit;

		System.out.println(query);

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setDouble(1, latitude);
		preparedStatement.setDouble(2, longitude);
		preparedStatement.setString(3, arrivalDateTime);
		preparedStatement.setString(4, arrivalDateTime);

		int index = 5;

		if (maxPrice != null) {
			preparedStatement.setDouble(index, maxPrice);
			index++;
		}

		if (radius != null) {
			preparedStatement.setInt(index, radius);
			index++;
		}

		return preparedStatement;
	}

	public class CarParkWrapper {
		public int id;
		public Double latitude;
		public Double longitude;
		public int capacity;
		public Double price_per_minute;
		public String address;
		public Double distance_to_destination;
		public int available;

	}
}
