package ws;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import database.PlainMysqlDatabaseDriver;
import flexjson.JSONException;

@Path("/payment")
public class PaymentProxyService {

	static final Logger ppLogger = LogManager.getLogger(PaymentProxyService.class.getName());
	protected Connection dbConn;

	@GET
	@Produces("application/json")
	public String handlePayment(
			@QueryParam("username") String username,
			@QueryParam("password") String password,
			@QueryParam("cardNumber")String cardNumber,
			@QueryParam("amount")Float amount) {

		if (username == null || password == null || cardNumber == null || amount == null) {
			ppLogger.error("Missing required attributes");
			return "Missing required attributes";
		}

		try {
			dbConn = (new PlainMysqlDatabaseDriver()).getConnection();

			ResultSet userResult = getUser(username);
			String passwordHash = "";
			int userId = -1;
			if (userResult.first()) {
				passwordHash = userResult.getString("password");
				userId = userResult.getInt("id");
			}

			AuthUtil authUtil = new AuthUtil();
			if (!authUtil.authenticate(password.toCharArray(), passwordHash)) {
				return "Not valid credentials";
			}

	
			else{
					PaymentDetails paymentDetails = new PaymentDetails();
					paymentDetails.userId = userResult.getInt("id");
					paymentDetails.cardNumber = cardNumber;
					paymentDetails.amount = amount;
					//PaymentDetails paymentDetails = new JSONDeserializer<PaymentDetails>().deserialize(input);
					if (collectPayment(paymentDetails)) {
						//SendEmail sm = new SendEmail();
						//String name = userResult.getString("first_name");
						//sm.SendMail(username,"Car Park Payment Confirmation","Hi " + name + ",\n\tYour payment has been done correctly.\n\tThe total amount payed has been " + amount+"\n\nYour friends,\nCarPark Developper Team.");
						return "OK";
					} else {
						String errorMessage = "Failed to process the payment. Payment details incorrect or insufficient funds.";
						ppLogger.error(errorMessage);
						return errorMessage;
					}
				}
			
		} catch (JSONException e) {
			String errorMessage = "Failed to process the payment. Invalid JSON input string.";
			ppLogger.error(errorMessage, e);
			return errorMessage;
		} catch (Exception e) {
			String errorMessage = "Failed to process the payment. Something went wrong.";
			ppLogger.error(errorMessage, e);
			return errorMessage;
		}
	}

	private ResultSet getUser(String username) throws SQLException {
		String query = "SELECT `id`, `password`,`first_name` " + "FROM User WHERE email = ? LIMIT 1";
		System.out.println(query);

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setString(1, username);

		return preparedStatement.executeQuery();
	}

	private boolean collectPayment(PaymentDetails paymentDetails) throws SQLException, ParseException {

		String insertTableSQL = "INSERT INTO Payment " + "(`user_id`, `card_number`, `amount`) "
				+ "VALUES (?,?,?)";
		PreparedStatement preparedStatement = dbConn.prepareStatement(insertTableSQL);
		preparedStatement.setInt(1, paymentDetails.userId);
		preparedStatement.setString(2, paymentDetails.cardNumber);
		preparedStatement.setFloat(3, paymentDetails.amount);

		// execute insert SQL stetement
		int rowCount = preparedStatement.executeUpdate();

		return rowCount == 1;
	}

	public static class PaymentDetails {
		public int userId;
		public String cardNumber;
		public float amount;
	}
}
