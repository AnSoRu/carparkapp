package ws;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.GET;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import database.PlainMysqlDatabaseDriver;
import email.SendEmail;

@Path("/booking")
public class BookingService {

	static final Logger bsLogger = LogManager.getLogger(BookingService.class.getName());
	protected Connection dbConn;

	@GET
	@Produces("application/json")
	public String list(
			@QueryParam("username") String username, 
			@QueryParam("password") String password,
			@QueryParam("carParkId") int carParkId,
			@QueryParam("bookingCost") Double bookingCost,
			@QueryParam("carRegNumber") String carRegNumber,
			@QueryParam("carMake") String carMake,
			@QueryParam("carColor") String carColor, 
			@QueryParam("carType") String carType,
			@QueryParam("paymentType") String paymentType, 
			@QueryParam("startDateTime") String startDateTime,
			@QueryParam("endDateTime") String endDateTime) 
	{
		if (username == null || password == null || carRegNumber == null || carMake == null || carColor == null
				|| carType == null || paymentType == null || startDateTime == null || endDateTime == null) {

			bsLogger.error("Missing required attributes");
			return "Missing required attributes";
		}

		try {
			dbConn = (new PlainMysqlDatabaseDriver()).getConnection();

			ResultSet userResult = getUser(username);
			String passwordHash = "";
			int userId = -1;
			if (userResult.first()) {
				passwordHash = userResult.getString("password");
				userId = userResult.getInt("id");
			}

			AuthUtil authUtil = new AuthUtil();
			if (!authUtil.authenticate(password.toCharArray(), passwordHash)) {
				return "Not valid credentials";
			}

			if (checkCarParkAvailability(carParkId, startDateTime, endDateTime)) {
				String[] params = { carRegNumber, carMake, carColor, carType, paymentType, startDateTime, endDateTime };
				if (saveBooking(userId, carParkId, bookingCost, params)) {
					SendEmail sm = new SendEmail();
					String name = userResult.getString("first_name");
					sm.SendMail(username, "Car Park Booking Confirmation", "Hi " + name + ",\nYour booking has been registered.\nPlease check out the following information:\n\t- CarNumber: " + carRegNumber +"\n\t- Car Park: " + carParkId + "\n\t- Price: " + bookingCost + "\n\t- Time\n\t\t-From: " + startDateTime + "\n\t\t To: "+ endDateTime + "\nIf you have any problem, please contact us at carparkappupm@gmail.com\n\nYour friends,\nCarPark Developper Team.");
					return "OK";
				} else {
					return "Error";
				}
			} else {
				bsLogger.error("Booking failed: Car park not available for selected date range");
				return "Car park not available for selected date range";
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			bsLogger.error("Failed to complete the booking", e);
			return "Failed to complete the booking";
		}
	}

	private ResultSet getUser(String username) throws SQLException {
		String query = "SELECT `id`, `password`,`first_name` " + "FROM User WHERE email = ? LIMIT 1";
		System.out.println(query);

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setString(1, username);

		return preparedStatement.executeQuery();
	}

	protected boolean checkCarParkAvailability(int carParkId, String startDateTime, String endDateTime)
			throws ParseException, SQLException {

		String query = "SELECT `id`, `lat`, `long`, `capacity`, `price_per_minute`, `address`, "
				+ "        (`capacity` - IFNULL( bb.`spaces_taken` , 0 )) AS `available` " + "FROM CarPark cp ";

		String join = "LEFT JOIN (SELECT COUNT(b.`id`) as `spaces_taken`, b.`CarPark_id` " + "FROM Booking b "
				+ "WHERE ((b.`booked_from` <= ? " + // booked_from <= START_DATE
													// AND
				"           AND b.`booked_to` > ?)" + // booked_to > START_DATE
				"       OR" + // OR
				"       (b.`booked_from` > ? " + // booked_from > START_DATE AND
				"           AND b.`booked_from` < ? )) " + // booked_from <
															// END_DATE
				"AND b.`CarPark_id` = ? " + "GROUP BY b.`CarPark_id`) bb " + "ON bb.`CarPark_id` = cp.`id` "
				+ "WHERE cp.`id` = ?";

		query = query + join;

		System.out.println(query);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

		Date parsedStartDate = dateFormat.parse(startDateTime);
		Timestamp timestampStart = new Timestamp(parsedStartDate.getTime());

		Date parsedEndDate = dateFormat.parse(endDateTime);
		Timestamp timestampEnd = new Timestamp(parsedEndDate.getTime());

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setTimestamp(1, timestampStart);
		preparedStatement.setTimestamp(2, timestampStart);
		preparedStatement.setTimestamp(3, timestampStart);
		preparedStatement.setTimestamp(4, timestampEnd);
		preparedStatement.setInt(5, carParkId);
		preparedStatement.setInt(6, carParkId);

		ResultSet rs = preparedStatement.executeQuery();
		if (rs.first()) {
			int spacesAvailable = rs.getInt("available");
			if (spacesAvailable > 0) {
				return true;
			}
		}

		return false;
	}

	protected boolean saveBooking(int userId, int carParkId, Double bookingCost, String[] bookingAttributes)
			throws SQLException {
		/*
		 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 * Date parsedDate = dateFormat.parse(accountProperties[5]); Timestamp
		 * timestamp = new Timestamp(parsedDate.getTime());
		 */

		String query = "INSERT INTO Booking " + "(`User_id`, `CarPark_id`, `estimated_cost`, `car_reg_number`, "
				+ "`car_make`, `car_color`, `car_type`, `payment_type`, `booked_from`, `booked_to`)  "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";

		System.out.println(query);

		PreparedStatement preparedStatement = dbConn.prepareStatement(query);
		preparedStatement.setInt(1, userId);
		preparedStatement.setInt(2, carParkId);
		preparedStatement.setDouble(3, bookingCost);
		preparedStatement.setString(4, bookingAttributes[0]);
		preparedStatement.setString(5, bookingAttributes[1]);
		preparedStatement.setString(6, bookingAttributes[2]);
		preparedStatement.setString(7, bookingAttributes[3]);
		preparedStatement.setString(8, bookingAttributes[4]);
		preparedStatement.setString(9, bookingAttributes[5]);
		preparedStatement.setString(10, bookingAttributes[6]);

		// execute insert SQL stetement
		int rowCount = preparedStatement.executeUpdate();

		return rowCount == 1;
	}
}