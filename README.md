# CarPark Server #

Este repositorio incluye una aplicación Java para la gestión de plazas de aparcamiento. Esta aplicación se corresponde con el proyecto de la asignatura de Profundización en Ingeniería de Software del Máster en Ingeniería Informática de la Universidad Politécnica de Madrid.

##Información
* Versión 3

##Equipo de desarrollo

* Alfonso Fernandez
* Ángel Soler
* Jose Manuel Carral
* Ivan Ribakovs
* Manuel Valbuena

##Servidor
Este servidor da soporte a una aplicación Android a través de servicios web. Utiliza una base de datos MySQL y está deplegada en un servidor Tomcat v8.

##Instrucciones pare el uso de GIT
Para clonar el repositorio:
---------------------------
* `$ git clone https://AnSoRu@bitbucket.org/AnSoRu/carparkapp.git`

##Importación

Importar el proyecto en un IDE para Java. En nuestro caso hemos utilizado Eclipse.